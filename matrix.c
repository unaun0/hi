#include "../inc/matrix.h"

int matrix_allocate(matrix_t *matrix, const size_t n, const size_t m)
{
    int rc = OK;
    
    matrix->data = calloc(n, sizeof(int *));
    if (!(matrix->data))
        rc = ERR_MEM;
    for (size_t i = 0; i < n && rc == OK; i++)
    {
        matrix->data[i] = calloc(n, sizeof(int));
        if (!(matrix->data[i]))
        {
            matrix->rows = i;
            matrix_free(matrix);
            rc = ERR_MEM;
        }
    }
    if (!rc)
    {
        matrix->rows = n;
        matrix->columns = m;
    }
    
    return rc;
}

void matrix_free(matrix_t *matrix)
{
    for (size_t i = 0; i < matrix->rows; i++)
        free(matrix->data[i]);
    free(matrix->data);
}

int matrix_read(matrix_t *matrix, FILE *stream)
{
    int rc = OK;
    if (!stream)
        rc = ERR_FILE;
    if (!rc)
    {
        size_t n, m;
        if (fscanf(stream, "%zu%zu", &n, &m) != 2)
            return ERR_FILE;
        rc = matrix_allocate(matrix, n, m);
        int elem;
        for (size_t i = 0; i < n && rc == OK; i++)
        {
            for (size_t j = 0; j < m && rc == OK; j++)
            {
                if (fscanf(stream, "%d", &elem) != 1)
                    rc = ERR_FILE;
                else
                    matrix->data[i][j] = elem;
            }
        }
        if (rc)
            matrix_free(matrix);
    }
    return rc;
}

int matrix_read_from_file(matrix_t *matrix, const char *filename)
{
    int rc = OK;
    
    FILE *file = fopen(filename, "r");
    if (!file)
        rc = ERR_FILE;
    else
    {
        rc = matrix_read(matrix, file);
        fclose(file);
    }
    return rc;
}

void matrix_print(FILE *stream, const matrix_t matrix)
{
    if (!stream)
        return ;
    for (size_t i = 0; i < matrix.rows; ++i)
    {
        for (size_t j = 0; j < matrix.columns; ++j)
            fprintf(stream, "%d ", matrix.data[i][j]);
        fprintf(stream, "\n");
    }
}

int matrix_print_to_file(const char *filename, const matrix_t matrix)
{
    int rc = OK;
    
    FILE *file = fopen(filename, "w");
    if (!file)
        rc = ERR_FILE;
    else
    {
        matrix_print(file, matrix);
        fclose(file);
    }
    
    return rc;
}

int is_zero_row(const matrix_t matrix, const size_t ind)
{
    int rc = OK;
    for (size_t i = 0; i < matrix.columns && rc == OK; i++)
        if (matrix.data[ind][i] != 0)
            rc = ERR;
    return rc;
}

int delete_row(matrix_t *matrix, const size_t ind)
{
    int rc = OK;
    free(matrix->data[ind]);
    for (size_t i = ind; i < matrix->rows - 1; i++)
        matrix->data[i] = matrix->data[i + 1];
    matrix->rows--;
    matrix->data = realloc(matrix->data, matrix->rows * sizeof(int *));
    if (!matrix->data)
        rc = ERR_MEM;
    return rc;
}

int process_matrix_rows
 (matrix_t *matrix,
  int (*process)(matrix_t *, const size_t),
  int (*attribute)(const matrix_t, const size_t))
{
    int rc = OK;
    for (int i = matrix->rows - 1; i >= 0 && rc == OK; i--)
    {
        if (!(attribute(*matrix, i)))
            rc = process(matrix, i);
    }
    return rc;
}
