#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "err.h"

typedef struct
{
    int **data;
    size_t rows;
    size_t columns;
} matrix_t;

// Memory
int matrix_allocate(matrix_t *matrix, const size_t n, const size_t m);
void matrix_free(matrix_t *matrix);

//IO
int matrix_read(matrix_t *matrix, FILE *stream);
void matrix_print(FILE *stream, const matrix_t matrix);
int matrix_print_to_file(const char *filename, matrix_t matrix);
int matrix_read_from_file(matrix_t *matrix, const char *filename);

// Process
int delete_row(matrix_t *matrix, const size_t ind);
int is_zero_row(const matrix_t matrix, const size_t ind);
int process_matrix_rows
 (matrix_t *matrix,
  int (*process)(matrix_t *, const size_t),
  int (*attribute)(const matrix_t, const size_t));

#endif
