CC := gcc

INC_DIR := ./inc
SRC_DIR := ./src
OBJ_DIR := ./out

INC_FILES := $(wildcard $(INC_DIR)/*.h)
SRC_FILES = $(wildcard $(SRC_DIR)/*.c)
OBJ_FILES := $(SRC_FILES:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

EXEC_APP = app.exe

CFLAGS := -std=c99 -Wall -Werror -Wpedantic -Wextra -Wfloat-conversion -Wfloat-equal -I $(INC_DIR)
OFLAGS := -lm -I $(INC_DIR)

RM := rm -rf

release: CFLAGS += -g0
release: .releaselast | app.exe

.releaselast:
	touch .releaslast
	$(RM) $(OBJ_DIR) .releaselast

$(EXEC_APP) : $(OBJ_DIR) $(OBJ_FILES)
	$(CC) $(OBJ_FILES) $(OFLAGS) -o $@

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -MMD -c $< -o $@

$(OBJ_DIR) :
	mkdir $@

.PHONY : clean

clean:
	@rm -rf $(OBJ_DIR) *.exe *.txt .releaselast;

