#include "../inc/matrix.h"

#define INIT {NULL, 0, 0}

#define IN_FILE "in.txt"
#define OUT_FILE "out.txt"

int main(void)
{
    int rc = OK;
    
    matrix_t matrix = INIT;
    
    rc = matrix_read_from_file(&matrix, IN_FILE);
    if (!rc)
    {
        if (!(rc = process_matrix_rows(&matrix, delete_row, is_zero_row)))
            rc = matrix_print_to_file(OUT_FILE, matrix);
        matrix_free(&matrix);
    }
    return rc;
}
